package com.android_test.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android_test.R;
import com.android_test.utils.Constants;
import com.android_test.worker.Worker;
import com.squareup.picasso.Picasso;

/**
 * Created by sandstranger on 25.11.2017.
 */

public class WorkerInfoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workerinfo);
        loadWorkerInfo();
    }

    private void loadWorkerInfo() {
        Intent intent = getIntent();
        String workerName = intent.getStringExtra(Constants.WORKER_NAME_INTENT_EXTRA_KEY);
        if (workerName.isEmpty()) {
            finish();
            return;
        }
        Worker worker = Worker.findWorkerByName(workerName);
        setTextViewText(R.id.workerInfoNameTextView, worker.getName());
        setTextViewText(R.id.workerInfoSurNameTextView, worker.getSurName());
        setTextViewText(R.id.specialityWorkerInfoTextView, worker.getSpecialityName());
        setTextViewText(R.id.ageInfoTextView, worker.getWorkerAge());
        setTextViewText(R.id.birthdayTextView, worker.formattedBirthDay());

        if (worker.getAvatarUrl() != null && !worker.getAvatarUrl().isEmpty() && !worker.getAvatarUrl().equals("null")) {
            ImageView imageView = (ImageView) findViewById(R.id.image);
            Picasso.with(this).load(worker.getAvatarUrl()).into(imageView);
        }
    }

    private void setTextViewText(int textViewId, String text) {
        TextView textView = (TextView) findViewById(textViewId);
        textView.setText(text);
    }
}
