package com.android_test.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.android_test.R;
import com.android_test.ui.adapter.WorkersAdapter;
import com.android_test.utils.Constants;

/**
 * Created by sandstranger on 25.11.2017.
 */

public class WorkersActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        showListView();
    }

    private void showListView(){
        ListView listView = (ListView) findViewById(R.id.ListView);
        listView.setVisibility(View.VISIBLE);
        Intent intent = getIntent();
        listView.setAdapter(new WorkersAdapter(WorkersActivity.this,intent.getStringExtra(Constants.WORKER_SPECIALITY_INTENT_EXTRA_KEY)));
    }
}
