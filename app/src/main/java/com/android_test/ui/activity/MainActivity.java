package com.android_test.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.android_test.R;
import com.android_test.ui.adapter.SpecialityAdapter;
import com.android_test.worker.Worker;
import com.android_test.worker.WorkerSpeciality;
import com.android_test.worker.WorkersParser;

import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        loadWorkersData();
    }

    private void loadWorkersData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                WorkersParser.loadWorkersFromServer();
                ShowSpecialityListView();
            }
        }).start();
    }

    private void ShowSpecialityListView() {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                List<Worker> workerList = WorkersParser.getWorkers();
                if (workerList.isEmpty()) {
                    return;
                }
                HashMap<String, WorkerSpeciality> workerSpecialitiesSet = new HashMap<>();
                for (Worker worker : workerList) {
                    if (!workerSpecialitiesSet.containsKey(worker.getSpecialityName())) {
                        workerSpecialitiesSet.put(worker.getSpecialityName(), new WorkerSpeciality(worker.getSpecialityId(), worker.getSpecialityName()));
                    }
                }
                ListView listView = (ListView) findViewById(R.id.ListView);
                listView.setVisibility(View.VISIBLE);
                SpecialityAdapter specialityAdapter = new SpecialityAdapter(
                        MainActivity.this, workerSpecialitiesSet.values().toArray(new WorkerSpeciality[workerSpecialitiesSet.size()]));
                listView.setAdapter(specialityAdapter);
            }
        });
    }
}
