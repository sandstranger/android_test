package com.android_test.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android_test.R;
import com.android_test.ui.activity.WorkerInfoActivity;
import com.android_test.utils.Constants;
import com.android_test.worker.Worker;
import com.android_test.worker.WorkersParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandstranger on 25.11.2017.
 */

public class WorkersAdapter extends BaseAdapter {
    private List<Worker> workersListToShow = new ArrayList<Worker>() ;
    private Context context;

    public WorkersAdapter (Context context,String specialityNameToShow){
        this.context = context;
        List <Worker> workerList = WorkersParser.getWorkers();
        for (Worker worker: workerList) {
            if (worker.getSpecialityName().equals(specialityNameToShow)){
                workersListToShow.add(worker);
            }
        }
    }

    @Override
    public int getCount() {
        return workersListToShow.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.workerrow, viewGroup, false);
        final Worker worker  = workersListToShow.get(i);
        TextView workerNameTextView = (TextView) rowView.findViewById(R.id.workerNameTextView);
        workerNameTextView.setText(worker.getName());
        TextView workerSurNameTextView = (TextView) rowView.findViewById(R.id.workerSurNameTextView);
        workerSurNameTextView.setText(worker.getSurName());
        TextView workerAgeView = (TextView) rowView.findViewById(R.id.ageTextView);
        workerAgeView.setText(""+ worker.getWorkerAge());
        Button btnShowWorkerInfo = (Button) rowView.findViewById(R.id.btnShowWorkerInfo);
        btnShowWorkerInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, WorkerInfoActivity.class);
                intent.putExtra(Constants.WORKER_NAME_INTENT_EXTRA_KEY,worker.getName());
                context.startActivity(intent);
            }
        });
        return rowView;
    }
}
