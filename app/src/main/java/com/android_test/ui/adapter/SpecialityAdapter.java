package com.android_test.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android_test.R;
import com.android_test.ui.activity.WorkersActivity;
import com.android_test.utils.Constants;
import com.android_test.worker.WorkerSpeciality;

/**
 * Created by sandstranger on 25.11.2017.
 */

public class SpecialityAdapter extends BaseAdapter {
    private WorkerSpeciality[] workerSpecialityArray = new WorkerSpeciality[0];
    private Context context;

    public SpecialityAdapter(Context context, WorkerSpeciality[] workerSpecialityArray) {
        this.context = context;
        if (workerSpecialityArray != null) {
            this.workerSpecialityArray = workerSpecialityArray;
        }
    }

    @Override
    public int getCount() {
        return workerSpecialityArray.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.specialityrow, viewGroup, false);
        TextView textView = (TextView) rowView.findViewById(R.id.specialityNameTextView);
        WorkerSpeciality speciality = workerSpecialityArray[i];
        textView.setText(speciality.getSpecialityName());
        Button btnShowWorkers = (Button) rowView.findViewById(R.id.btnShowWorkes);
        btnShowWorkers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, WorkersActivity.class);
                intent.putExtra(Constants.WORKER_SPECIALITY_INTENT_EXTRA_KEY,workerSpecialityArray[i].getSpecialityName());
                context.startActivity(intent);
            }
        });
        return rowView;
    }
}
