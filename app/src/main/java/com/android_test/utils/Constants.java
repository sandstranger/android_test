package com.android_test.utils;

/**
 * Created by sandstranger on 25.11.2017.
 */

public class Constants {
    public static final String REQUEST_URL_PATH = "http://gitlab.65apps.com/65gb/static/raw/master/testTask.json";
    public static final String WORKER_SPECIALITY_INTENT_EXTRA_KEY = "worker_speciality";
    public static final String WORKER_NAME_INTENT_EXTRA_KEY = "worker_name";
}
