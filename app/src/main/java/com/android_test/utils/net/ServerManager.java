package com.android_test.utils.net;

import android.util.Log;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by sandstranger on 25.11.2017.
 */

public class ServerManager {

    public static final String getSyncServerRequest(String serverUrl) {
        String serverResponce = "";
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(serverUrl)
                    .build();
            Response response = client.newCall(request).execute();
            serverResponce = response.body().string();
        } catch (Exception e) {
            Log.e("Ecxeption","",e);
        }
        return serverResponce;
    }
}
