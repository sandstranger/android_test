package com.android_test.worker;

import android.util.Log;

import com.android_test.utils.Constants;
import com.android_test.utils.net.ServerManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import java.util.List;

public class WorkersParser {

    public static void loadWorkersFromServer() {
        try {
            String jsonString = ServerManager.getSyncServerRequest(Constants.REQUEST_URL_PATH);
            if (!jsonString.isEmpty()) {
                com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                JsonElement element = jsonParser.parse(jsonString);
                JsonArray obj = element.getAsJsonObject().getAsJsonArray("response");
                Gson gson = new Gson();
                saveWorkersToDB(gson.fromJson(obj, Worker[].class));
            }
        }
        catch (Exception e){
            Log.e("Exception","",e);
        }
    }

    private static void saveWorkersToDB (Worker[] workerArray){
        for (int i = 0; i<workerArray.length;i++) {
            Worker worker = workerArray[i];
            worker.updateAndSave();
        }
    }

    public static List<Worker> getWorkers() {
        return Worker.listAll(Worker.class);
    }
}
