package com.android_test.worker;

import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by sandstranger on 25.11.2017.
 */

public class Worker extends SugarRecord<Worker> {

    @SerializedName("f_name")
    private String name = "";
    @SerializedName("l_name")
    private String surName = "";
    @SerializedName("birthday")
    private String birthday = "";
    @SerializedName("avatr_url")
    private String avatarUrl = "";
    @SerializedName("specialty")
    private WorkerSpeciality[] workersSpecialities = new WorkerSpeciality[0];
    private int specialityId = 0;
    private String specialityName = "";

    public Worker() {
    }

    public String formattedBirthDay() {
        if (birthday != null && !birthday.isEmpty()) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
            try {
                Date date = formatter.parse(birthday);
                return new SimpleDateFormat("dd.mm.yyyy").format(date);
            } catch (ParseException e) {
            }
        }
        return "-";
    }

    public String getWorkerAge() {
        if (birthday != null && !birthday.isEmpty()) {
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                Date currentDate = new Date();
                Date date = formatter.parse(birthday);
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(currentDate.getTime() - date.getTime());
                return String.valueOf(c.get(Calendar.YEAR) - 1970);
            } catch (Exception e) {
                Log.e("EXCEPETION", "", e);
            }
        }
        return "-";
    }

    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public WorkerSpeciality[] getWorkersSpecialitiesArray() {
        return workersSpecialities != null ? workersSpecialities : new WorkerSpeciality[0];
    }

    public int getSpecialityId() {
        return specialityId;
    }

    public String getSpecialityName() {
        return specialityName;
    }

    public void updateAndSave() {
        Worker worker = findWorkerByName(name);
        if (!worker.getSpecialityName().isEmpty()) {
            worker.name = name;
            worker.surName = surName;
            worker.birthday = birthday;
            worker.avatarUrl = avatarUrl;
            if (workersSpecialities.length > 0) {
                worker.specialityId = workersSpecialities[0].getSpecialityId();
                worker.specialityName = workersSpecialities[0].getSpecialityName();
            }
            worker.save();
        } else {
            if (workersSpecialities.length > 0) {
                specialityId = workersSpecialities[0].getSpecialityId();
                specialityName = workersSpecialities[0].getSpecialityName();
            }
            save();
        }
    }

    public static Worker findWorkerByName(String name) {
        List<Worker> workerList = Worker.findWithQuery(Worker.class, "SELECT * FROM Worker WHERE name = ? LIMIT 1", name);
        return workerList != null && !workerList.isEmpty() ? workerList.get(0) : new Worker();
    }
}
