package com.android_test.worker;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sandstranger on 25.11.2017.
 */

public class WorkerSpeciality {

    @SerializedName("specialty_id")
    private int specialityId = 0;

    @SerializedName("name")
    private String specialityName = "";

    public WorkerSpeciality(int specialityId, String specialityName){
        this.specialityName = specialityName;
        this.specialityId = specialityId;
    }

    public int getSpecialityId (){
        return specialityId;
    }

    public String getSpecialityName (){
        return specialityName;
    }
}
